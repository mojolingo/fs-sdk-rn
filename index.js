'use strict';

// import RTCPeerConnection from './RTCPeerConnection';
// import RTCIceCandidate from './RTCIceCandidate';
// import RTCSessionDescription from './RTCSessionDescription';
import RTCView from './RTCView';
// import MediaStream from './MediaStream';
// import MediaStreamTrack from './MediaStreamTrack';
// import getUserMedia from './getUserMedia';
import methods from './src/SignalWireSDK';
export default methods

export {
//   RTCPeerConnection,
//   RTCIceCandidate,
//   RTCSessionDescription,
  RTCView,
//   MediaStream,
//   MediaStreamTrack,
//   getUserMedia,
//   methods,
}
