import { Verto } from './verto/verto'

const _instance = null
const HASH = {
  '1073ab6cda4b991cd29f9e83a307f34004ae9327': {
    name: 'APIKEY 1',
    login: '1008@fstest.mojolingo.io',
    passwd: '9999',
    socketUrl: 'wss://fstest.mojolingo.io:8082',
  },
  '87ba78e0f03afcef60657f342ec5567368fadd8c': {
    name: 'APIKEY 2',
    login: '1009@fstest.mojolingo.io',
    passwd: '9999',
    socketUrl: 'wss://fstest.mojolingo.io:8082',
  },
  '3b88ea816c78ec104041a75e78f32ec804eaac39': {
    name: 'APIKEY 3',
    login: '1010@fstest.mojolingo.io',
    passwd: '9999',
    socketUrl: 'wss://fstest.mojolingo.io:8082',
  },
  'c34bf5a9ecca6edc3128018b1dd235a0f7bdff20': {
    name: 'APIKEY 4',
    login: '1011@fstest.mojolingo.io',
    passwd: '9999',
    socketUrl: 'wss://fstest.mojolingo.io:8082',
  },
}

class SignalWireSDK {
  constructor(apikey, callbacks={}) {
    this.verto = {}
    this.params = HASH[apikey]
    this.callbacks = callbacks
    this.currentCall = null
    console.log('SW Params: ', this.params)
    Verto.init({
      skipPermCheck: true,
      skipDeviceCheck: true,
    }, this.login.bind(this))
  }

  login() {
    let self = this
    new Verto({
      name: this.params.name,
      login: this.params.login,
      passwd: this.params.passwd,
      socketUrl: this.params.socketUrl,
      // tag: MCU_ID,
      deviceParams: {
        useCamera: 'any',
        useMic: 'any',
        useSpeak: 'any',
      },
    }, {
      onWSLogin: (verto, success) => {
        if (success) {
          console.log('onWSLogin Success!')
          self.verto = verto
        } else {
          console.warn('onWSLogin Failure!')
        }
        if (self.callbacks.hasOwnProperty('onLogin')) {
          self.callbacks.onLogin(success)
        }
      },
      onDialogState: (dialog) => {
        console.log('onDialogState', dialog.params)
        switch (dialog.state.name)
        {
          case 'active':
            console.log('remoteStream: ', dialog.rtc.remoteStream)
            if (self.callbacks.hasOwnProperty('onRemoteStream')) {
              self.callbacks.onRemoteStream(dialog.rtc.remoteStream)
            }
          break
          case 'destroy':
            if (self.callbacks.hasOwnProperty('onDialogDestroy')) {
              self.callbacks.onDialogDestroy()
            }
          break
        }
      },
      onMessage: (verto, dialog, message, data) => {
        console.log('onMessage', message)
      },
    })
  }

  makeCall(extension, callbacks = null) {
    console.log('makeCall', extension)
    // this.verto.videoParams({

    // })
    this.currentCall = this.verto.newCall({
      login: this.params.login,
      destination_number: extension,
      caller_id_name: this.params.name,
      caller_id_number: this.params.login,
      remote_caller_id_name: extension,
      remote_caller_id_number: extension,
      outgoingBandwidth: 'default',
      incomingBandwidth: 'default',
      useVideo: true,
      useStereo: true,
      useCamera: 'any',
      dedEnc: true,
      userVariables: {},
    }, callbacks)
  }

  hangup() {
    if (this.currentCall) {
      this.currentCall.hangup()
    }
  }
}

export default {
  connect: (apikey, callbacks) => {
    console.log('CONNECT APIKEY', apikey)
    _instance = new SignalWireSDK(apikey, callbacks)
  },
  call: (extension) => {
    _instance.makeCall(extension)
  },
  hangup: () => {
    _instance.hangup()
  },
}